<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:input type="hidden" path="id" />
<form:input type="hidden" path="userId" />

<div class="write-content" align="center">

    <div><p>Title:</p></div>
    <div><form:input type="text" path="title" /></div>

    <div><p>Description:</p></div>
    <div><form:input type="text" path="description" /></div>

    <div><p>Status:</p></div>
    <div>
        <form:select path="status">
            <form:options items="${statuses}" itemLabel="title" />
        </form:select>
    </div>

    <div>Start date:</div>
    <div><form:input type="date" path="timeFrame.startDate" /></div>

    <div>End date:</div>
    <div><form:input type="date" path="timeFrame.endDate" /></div>

    <button type="submit">SAVE</button>

</div>