<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../../include/_header.jsp" />

<div class="list_header-content">
    <h1>Task list:</h1>
</div>

<div class="main-content">
    <table>

        <tr>
            <th width="200" nowrap="nowrap">ID</th>
            <th width="200" nowrap="nowrap">TITLE</th>
            <th width="200">PROJECT</th>
            <th width="100%">DESCRIPTION</th>
            <th width="150">STATUS</th>
            <th width="150">START DATE</th>
            <th width="150">END DATE</th>
            <th width="80" nowrap="nowrap">VIEW</th>
            <th width="80" nowrap="nowrap">EDIT</th>
            <th width="80" nowrap="nowrap">DELETE</th>
        </tr>

        <c:forEach var="task" items="${tasks}">
            <tr>
                <td>
                    <c:out value="${task.id}" />
                </td>

                <td>
                    <c:out value="${task.title}" />
                </td>

                <td>
                    <c:out value="${task.project.title}" />
                </td>

                <td>
                    <c:out value="${task.description}" />
                </td>

                <td>
                    <c:out value="${task.status.title}" />
                </td>

                <td>
                    <fmt:formatDate value="${task.timeFrame.startDate}" pattern="yyyy-MM-dd" />
                </td>

                <td>
                    <fmt:formatDate value="${task.timeFrame.endDate}" pattern="yyyy-MM-dd" />
                </td>

                <td>
                    <a href="/task/view/${task.id}"><button>View</button></a>
                </td>

                <td>
                    <a href="/task/edit/${task.id}"><button>Edit</button></a>
                </td>

                <td>
                    <a href="/task/delete/${task.id}"><button>Delete</button></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<div class="create_link-content">
    <a href="${pageContext.request.contextPath}/task/create/">
        <button>CREATE</button>
    </a>
</div>

<jsp:include page="../../include/_footer.jsp" />