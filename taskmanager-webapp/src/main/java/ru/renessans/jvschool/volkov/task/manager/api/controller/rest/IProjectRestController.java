package ru.renessans.jvschool.volkov.task.manager.api.controller.rest;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;

import java.util.Collection;

@RequestMapping("/api/projects")
public interface IProjectRestController {

    @NotNull
    @GetMapping
    Collection<ProjectDTO> projects();

    @Nullable
    @RequestMapping(value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    ProjectDTO create(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO
    );

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    ProjectDTO view(
            @PathVariable("id") @NotNull String id
    );

    @DeleteMapping("/project/{id}")
    int delete(
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @SneakyThrows
    @PostMapping("/project/edit/{id}")
    ProjectDTO edit(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO
    );

}