package ru.renessans.jvschool.volkov.task.manager.api.controller.rest;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import java.util.Collection;

@RequestMapping("/api/tasks")
public interface ITaskRestController {

    @NotNull
    @GetMapping
    Collection<TaskDTO> tasks();

    @Nullable
    @RequestMapping(value = "/task/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    TaskDTO create(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO
    );

    @Nullable
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    TaskDTO view(
            @PathVariable("id") @NotNull String id
    );

    @DeleteMapping("/task/{id}")
    int delete(
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @SneakyThrows
    @PostMapping("/task/edit/{id}")
    TaskDTO edit(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO
    );

}