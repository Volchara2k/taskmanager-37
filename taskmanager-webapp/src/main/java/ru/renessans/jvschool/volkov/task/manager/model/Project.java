package ru.renessans.jvschool.volkov.task.manager.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractUserOwner {

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.REMOVE,
            orphanRemoval = true
    )
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Task> tasks = new ArrayList<>();

    public Project(
            @Nullable final String id
    ) {
        setId(id);
    }

    public Project(
            @NotNull final String title,
            @NotNull final String description
    ) {
        setTitle(title);
        setDescription(description);
    }

    public Project(
            @NotNull final String userId,
            @NotNull final String title,
            @NotNull final String description
    ) {
        setUserId(userId);
        setTitle(title);
        setDescription(description);
    }

}