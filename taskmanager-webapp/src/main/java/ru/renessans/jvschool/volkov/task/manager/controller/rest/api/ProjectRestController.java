package ru.renessans.jvschool.volkov.task.manager.controller.rest.api;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.controller.rest.IProjectRestController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/projects",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ProjectRestController implements IProjectRestController {

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping
    @Override
    public Collection<ProjectDTO> projects() {
        return this.projectUserService.exportOwnerUser()
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @RequestMapping(value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @Override
    public ProjectDTO create(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        try {
            this.projectUserService.addOwnerUser(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @Override
    public ProjectDTO view(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.projectUserService.getRecordById(id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        return this.projectAdapterService.toDTO(project);
    }

    @DeleteMapping("/project/{id}")
    @Override
    public int delete(
            @PathVariable("id") @NotNull final String id
    ) {
        return this.projectUserService.cascadeDeleteRecordById(id);
    }

    @Nullable
    @SneakyThrows
    @RequestMapping(value = "/project/edit/{id}",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @Override
    public ProjectDTO edit(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        try {
            this.projectUserService.addOwnerUser(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

}