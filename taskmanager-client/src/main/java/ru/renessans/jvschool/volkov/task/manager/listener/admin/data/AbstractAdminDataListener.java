package ru.renessans.jvschool.volkov.task.manager.listener.admin.data;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;

@RequiredArgsConstructor
public abstract class AbstractAdminDataListener extends AbstractListener {

    @NotNull
    protected final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}