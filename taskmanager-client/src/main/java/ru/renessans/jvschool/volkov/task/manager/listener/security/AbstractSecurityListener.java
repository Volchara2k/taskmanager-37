package ru.renessans.jvschool.volkov.task.manager.listener.security;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;

@RequiredArgsConstructor
public abstract class AbstractSecurityListener extends AbstractListener {

    @NotNull
    protected final SessionEndpoint sessionEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}