package ru.renessans.jvschool.volkov.task.manager.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.SystemUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class HardwareDataListener extends AbstractListener {

    @NotNull
    private static final String CMD_INFO = "info";

    @NotNull
    private static final String ARG_INFO = "-i";

    @NotNull
    private static final String DESC_INFO = "вывод информации о системе";

    @NotNull
    private static final String NOTIFY_INFO = "Информация о системе: \n";

    @NotNull
    @Override
    public String command() {
        return CMD_INFO;
    }

    @NotNull
    @Override
    public String argument() {
        return ARG_INFO;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_INFO;
    }

    @Async
    @Override
    @EventListener(condition = "@hardwareDataListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_INFO);
        ViewUtil.print(SystemUtil.getHardwareDataAsString());
    }

}