package ru.renessans.jvschool.volkov.task.manager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class TaskClearListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_CLEAR = "task-clear";

    @NotNull
    private static final String DESC_TASK_CLEAR = "очистить все задачи";

    @NotNull
    private static final String NOTIFY_TASK_CLEAR = "Производится очистка списка задач...";

    public TaskClearListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_CLEAR;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_CLEAR;
    }

    @Async
    @Override
    @EventListener(condition = "@taskClearListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_CLEAR);
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final int deleteFlag = super.taskEndpoint.deleteAllTasks(current);
        ViewUtil.print(deleteFlag);
    }

}