package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataBase64ExportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_BASE64_EXPORT = "data-base64-export";

    @NotNull
    private static final String DESC_BASE64_EXPORT = "экспортировать домен в base64 вид";

    @NotNull
    private static final String NOTIFY_BASE64_EXPORT = "Происходит процесс выгрузки домена в base64 вид...";

    public DataBase64ExportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_BASE64_EXPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_BASE64_EXPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataBase64ExportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final DomainDTO exportDataBase64 = super.adminDataInterChangeEndpoint.exportDataBase64(current);
        ViewUtil.print(NOTIFY_BASE64_EXPORT);
        ViewUtil.print(exportDataBase64.getUsers());
        ViewUtil.print(exportDataBase64.getTasks());
        ViewUtil.print(exportDataBase64.getProjects());
    }

}
