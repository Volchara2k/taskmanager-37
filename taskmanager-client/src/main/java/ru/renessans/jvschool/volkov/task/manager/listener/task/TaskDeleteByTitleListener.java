package ru.renessans.jvschool.volkov.task.manager.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.TaskEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class TaskDeleteByTitleListener extends AbstractTaskListener {

    @NotNull
    private static final String CMD_TASK_DELETE_BY_TITLE = "task-delete-by-title";

    @NotNull
    private static final String DESC_TASK_DELETE_BY_TITLE = "удалить задачу по заголовку";

    @NotNull
    private static final String NOTIFY_TASK_DELETE_BY_TITLE =
            "Происходит попытка инициализации удаления задачи. \n" +
                    "Для удаления задачи по заголовку введите имя заголовок из списка. ";

    public TaskDeleteByTitleListener(
            @NotNull final TaskEndpoint taskEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(taskEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_TASK_DELETE_BY_TITLE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_TASK_DELETE_BY_TITLE;
    }

    @Async
    @Override
    @EventListener(condition = "@taskDeleteByTitleListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_TASK_DELETE_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final int deleteFlag = super.taskEndpoint.deleteTaskByTitle(current, title);
        ViewUtil.print(deleteFlag);
    }

}