package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataYamlImportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_YAML_IMPORT = "data-yaml-import";

    @NotNull
    private static final String DESC_YAML_IMPORT = "импортировать домен из yaml вида";

    @NotNull
    private static final String NOTIFY_YAML_IMPORT = "Происходит процесс загрузки домена из yaml вида...";

    public DataYamlImportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_YAML_IMPORT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_YAML_IMPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataYamlImportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataYaml(current);
        ViewUtil.print(NOTIFY_YAML_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}