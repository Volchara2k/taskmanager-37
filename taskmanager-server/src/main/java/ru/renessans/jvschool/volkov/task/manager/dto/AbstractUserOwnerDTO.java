package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.enumeration.Status;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractUserOwnerDTO extends AbstractDTO {

    @NotNull
    private String title = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId;

    @NotNull
    private TimeFrameDTO timeFrame = new TimeFrameDTO();

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Override
    public String toString() {
        return "Заголовок задачи: " + getTitle() +
                ", описание задачи - " + getDescription() +
                "\nИдентификатор: " + super.getId() + "\n";
    }

}