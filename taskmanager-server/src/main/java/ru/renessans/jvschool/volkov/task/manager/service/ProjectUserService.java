package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.repository.IProjectUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

@Service
@Transactional
public final class ProjectUserService extends AbstractUserOwnerService<Project> implements IProjectUserService {

    public ProjectUserService(
            @NotNull final IProjectUserRepository projectUserRepository,
            @NotNull final IUserService userService
    ) {
        super(projectUserRepository, userService);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Project addOwnerUser(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(description)) throw new InvalidDescriptionException();
        @NotNull final Project project = new Project(userId, title, description);
        return super.addOwnerUser(project);
    }

}