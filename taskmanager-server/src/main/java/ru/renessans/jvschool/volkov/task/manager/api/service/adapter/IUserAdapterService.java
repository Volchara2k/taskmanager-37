package ru.renessans.jvschool.volkov.task.manager.api.service.adapter;

import ru.renessans.jvschool.volkov.task.manager.dto.AbstractDTO;
import ru.renessans.jvschool.volkov.task.manager.model.User;

public interface IUserAdapterService<D extends AbstractDTO> extends IAdapterService<D, User> {
}