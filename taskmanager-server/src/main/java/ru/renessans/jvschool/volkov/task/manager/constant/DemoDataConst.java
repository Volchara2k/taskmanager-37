package ru.renessans.jvschool.volkov.task.manager.constant;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;

import java.util.Arrays;
import java.util.Collection;

public interface DemoDataConst {

    @NotNull
    String DEMO_TITLE_1ST = "exceptions change";

    @NotNull
    String DEMO_DESCRIPTION_1ST = "change the approach to exceptions";

    @NotNull
    String DEMO_TITLE_2ND = "rethink view";

    @NotNull
    String DEMO_DESCRIPTION_2ND = "rethink the view approach";

    @NotNull
    String USER_TEST_LOGIN = "test";

    @NotNull
    String USER_TEST_PASSWORD = "test";

    @NotNull
    String USER_DEFAULT_LOGIN = "user";

    @NotNull
    String USER_DEFAULT_PASSWORD = "user";

    @NotNull
    String USER_ADMIN_LOGIN = "admin";

    @NotNull
    String USER_ADMIN_PASSWORD = "admin";

    @NotNull
    String USER_MANAGER_LOGIN = "manager";

    @NotNull
    String USER_MANAGER_PASSWORD = "manager";

    @NotNull
    Collection<User> USERS = Arrays.asList(
            new User(
                    DemoDataConst.USER_TEST_LOGIN,
                    HashUtil.getSaltHashLine(DemoDataConst.USER_TEST_PASSWORD)
            ),
            new User(
                    DemoDataConst.USER_ADMIN_LOGIN,
                    HashUtil.getSaltHashLine(DemoDataConst.USER_ADMIN_PASSWORD),
                    UserRole.ADMIN
            ),
            new User(
                    DemoDataConst.USER_MANAGER_LOGIN,
                    HashUtil.getSaltHashLine(DemoDataConst.USER_MANAGER_PASSWORD)
            ),
            new User(
                    DemoDataConst.USER_DEFAULT_LOGIN,
                    HashUtil.getSaltHashLine(DemoDataConst.USER_DEFAULT_PASSWORD)
            )
    );

    @NotNull
    Collection<String> USERS_LOGINS = Arrays.asList(
            DemoDataConst.USER_TEST_LOGIN,
            DemoDataConst.USER_ADMIN_LOGIN,
            DemoDataConst.USER_MANAGER_LOGIN,
            DemoDataConst.USER_DEFAULT_LOGIN
    );

}