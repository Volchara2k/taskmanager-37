package ru.renessans.jvschool.volkov.task.manager.exception.invalid.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidEndpointsException extends AbstractException {

    @NotNull
    private static final String EMPTY_ENDPOINTS = "Ошибка! Параметр \"endpoints\" отсутствует!\n";

    public InvalidEndpointsException() {
        super(EMPTY_ENDPOINTS);
    }

}