# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create**](DefaultApi.md#create) | **PUT** /api/projects/project/create | 
[**create_0**](DefaultApi.md#create_0) | **POST** /api/projects/project/create | 
[**create_1**](DefaultApi.md#create_1) | **PUT** /api/tasks/task/create | 
[**create_2**](DefaultApi.md#create_2) | **POST** /api/tasks/task/create | 
[**delete**](DefaultApi.md#delete) | **DELETE** /api/projects/project/{id} | 
[**delete_0**](DefaultApi.md#delete_0) | **DELETE** /api/tasks/task/{id} | 
[**edit**](DefaultApi.md#edit) | **PUT** /api/projects/project/edit/{id} | 
[**edit_0**](DefaultApi.md#edit_0) | **POST** /api/projects/project/edit/{id} | 
[**edit_1**](DefaultApi.md#edit_1) | **PUT** /api/tasks/task/edit/{id} | 
[**edit_2**](DefaultApi.md#edit_2) | **POST** /api/tasks/task/edit/{id} | 
[**projects**](DefaultApi.md#projects) | **GET** /api/projects | 
[**tasks**](DefaultApi.md#tasks) | **GET** /api/tasks | 
[**view**](DefaultApi.md#view) | **GET** /api/projects/project/view/{id} | 
[**view_0**](DefaultApi.md#view_0) | **GET** /api/tasks/task/view/{id} | 


<a name="create"></a>
# **create**
> ProjectDTO create(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.create(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  | [optional]

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="create_0"></a>
# **create_0**
> ProjectDTO create_0(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.create_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  | [optional]

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="create_1"></a>
# **create_1**
> TaskDTO create_1(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.create_1(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  | [optional]

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="create_2"></a>
# **create_2**
> TaskDTO create_2(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.create_2(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#create_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  | [optional]

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="delete"></a>
# **delete**
> Integer delete(id)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Integer result = apiInstance.delete(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#delete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="delete_0"></a>
# **delete_0**
> Integer delete_0(id)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Integer result = apiInstance.delete_0(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#delete_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="edit"></a>
# **edit**
> ProjectDTO edit(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.edit(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#edit");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  | [optional]

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="edit_0"></a>
# **edit_0**
> ProjectDTO edit_0(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.edit_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#edit_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  | [optional]

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="edit_1"></a>
# **edit_1**
> TaskDTO edit_1(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.edit_1(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#edit_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  | [optional]

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="edit_2"></a>
# **edit_2**
> TaskDTO edit_2(body)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.edit_2(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#edit_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  | [optional]

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="projects"></a>
# **projects**
> List&lt;ProjectDTO&gt; projects()



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<ProjectDTO> result = apiInstance.projects();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#projects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ProjectDTO&gt;**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="tasks"></a>
# **tasks**
> List&lt;TaskDTO&gt; tasks()



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TaskDTO> result = apiInstance.tasks();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#tasks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TaskDTO&gt;**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="view"></a>
# **view**
> ProjectDTO view(id)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    ProjectDTO result = apiInstance.view(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#view");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="view_0"></a>
# **view_0**
> TaskDTO view_0(id)



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    TaskDTO result = apiInstance.view_0(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#view_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

